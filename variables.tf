variable "hostname" {}
variable "instance" {}

variable queue {
  default = "default-queue"
}

variable "threshold" {
  default = "1"
}