resource "aws_cloudwatch_metric_alarm" "autorecovery" {
  alarm_name          = "ec2-autorecover-${var.hostname}"
  namespace           = "AWS/EC2"
  evaluation_periods  = "5"
  period              = "60"
  alarm_description   = "This metric auto recovers EC2 instances"
  alarm_actions       = [
    "arn:aws:automate:us-west-2:ec2:recover",
    "${var.queue}",
  ]
  statistic           = "Minimum"
  comparison_operator = "GreaterThanThreshold"
  threshold           = "${var.threshold}"
  metric_name         = "StatusCheckFailed_System"

  dimensions {
    InstanceId = "${var.instance}"
  }
}
