# Terraform CloudWatch module

This is simple example of real-life TF module that should be migrated to separate git repository. This approach gives ability to avoid code duplication and code re-usability. Also one of great features is code versioning, which is accomplished with Tags.
